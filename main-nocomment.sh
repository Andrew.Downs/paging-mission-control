#!/bin/bash

export SATELLITES=''
export NEWSAT=''
export SAT_ID=''
export SAT_INPUT=''
export SAT_COLLECTOR=''
export SAT_INPUT=''
export SAT_TABLE_CURRENT_LINE=''
export TIMESTAMP=''
export EPOCH=''
export NEXT_EPOCH=''
export CURRENT=''
export DATE=''
export DATE_TEST=''
export ALARM_SAT_TABLE=''
export HEADCOUNT='2'
export LENGTH='5min'

alarm_output_start () {
echo "["
}

alarm_output_end () {
echo "]"
}

alarm_segment () {
echo '          satelliteId: '$SAT_ID','
echo '          severity: "'"$SEVERITY"'",'
echo '          component: "'"$COMPONENT"'",'
echo '          timestamp: "'"$JSON_YEAR-$JSON_MONTH-$JSON_DAY"T"$TIMESTAMP"Z""'"'
TOTAL_JSON_NODES=$((TOTAL_JSON_NODES-1))
}

alarm_bracketing_open () {
echo "     {"
}

alarm_bracketing_close () {
if [[ $TOTAL_JSON_NODES -gt 0 ]]
	then
		echo "     },"
	else
		echo "     }"
fi
}

DATES=$(cat input.db | cut -f1 -d " ")
DATE_TEST=$(echo "$DATES" | sort | uniq -u)
echo $DATE_TEST
if [[ $DATE_TEST == '' ]]
then
	ALL_DATES_SAME='1'
else
	echo "GENERATING DATE DIRECTORIES FOR NEW DATES"
fi
for DAY in $DATES
do
        if [[ $DAYS != *"$DAY"* ]]
        then
        	DAYS="$DAY $DAYS"
        fi
done

for CURRENT_DAY in $DAYS
do
	JSON_YEAR=${CURRENT_DAY:0:4}
	JSON_MONTH=${CURRENT_DAY:4:2}
	JSON_DAY=${CURRENT_DAY:6:2}
done

SAT_INPUT=$(cat input.db | cut -f2 -d "|")
for SAT_COLLECTOR in $SAT_INPUT
do
        if [[ $SATELLITES != *"$SAT_COLLECTOR"* ]]
        then
        	SATELLITES="$SAT_COLLECTOR $SATELLITES"
        fi
done

for SAT_ID in $SATELLITES
do

SAT_TABLE=$(cat input.db | grep "|$SAT_ID|" | cut -f2 -d " ")

	for SAT_TABLE_CURRENT_LINE in $SAT_TABLE
		do
		TIMESTAMP=${SAT_TABLE_CURRENT_LINE:0:12}
		if [[ NEXT_EPOCH != '' ]]
		then
		EPOCH=$NEXT_EPOCH
		fi
		if [[ $EPOCH == '' ]]
		then
			EPOCH=$TIMESTAMP
			CURRENT=$TIMESTAMP
			WINDOW=$(date -d "$EPOCH $LENGTH" +"%H:%M:%S.%3N")
		else
			CURRENT=$TIMESTAMP
		fi
                CURRENT_UTIME=$(date -u -d "$CURRENT" +"%s.%3N")
                WINDOW_UTIME=$(date -u -d "$WINDOW" +"%s.%3N")

			if (( $(echo "$CURRENT_UTIME < $WINDOW_UTIME" | bc -l) ))
			then
			COMPONENT=$(echo $SAT_TABLE_CURRENT_LINE | cut -f8 -d "|")

				case "$COMPONENT" in
						BATT)
							GET_RED_LOW=$(echo $SAT_TABLE_CURRENT_LINE | cut -f6 -d "|")
							GET_RAW=$(echo $SAT_TABLE_CURRENT_LINE | cut -f7 -d "|")
							if (( $(echo "$GET_RED_LOW > $GET_RAW" | bc -l) ))
								then
								BATT_ALARMDB="$SAT_TABLE_CURRENT_LINE $BATT_ALARMDB"
							fi
						;;
						TSTAT)
							GET_RED_HIGH=$(echo $SAT_TABLE_CURRENT_LINE | cut -f3 -d "|")
							GET_RAW=$(echo $SAT_TABLE_CURRENT_LINE | cut -f7 -d "|")
	        	            if (( $(echo "$GET_RED_HIGH < $GET_RAW" | bc -l) ))
                        		then
                               	TSTAT_ALARMDB="$SAT_TABLE_CURRENT_LINE $TSTAT_ALARMDB"
                            fi
						;;

							*)
						echo "COMPONENT TYPE UNRECOGNIZED"
				esac
			else
				echo "THIS TIMESTAMP IS OUTSIDE THE ALLOTED TIME WINDOW - IT WILL BE USED FOR THE NEXT ITERATION OF RULE EVALUATIONS"
				NEXT_EPOCH=$CURRENT
			fi
	done
export EPOCH=''
done
ALARM_TABLE=$TSTAT_ALARMDB" "$BATT_ALARMDB

export SAT_ID=''

for SAT_ID in $SATELLITES
do
	for ALARM_TABLE_CURRENT_LINE in $ALARM_TABLE
	do
		ALARM_SAT_COUNTER=$(echo $ALARM_TABLE_CURRENT_LINE | grep "|$SAT_ID|" )
		if  [[ $ALARM_SAT_COUNTER != '' ]]
		then
			SAT_COUNT=$((SAT_COUNT+1))
		fi
	done
if (( "$SAT_COUNT" >= "3" ))
then
	ALARM_PENDING_SAT_ID=$SAT_ID" "$ALARM_PENDING_SAT_ID
fi
export SAT_COUNT=''
done

export SAT_ID=''
export ALARM_TABLE_CURRENT_LINE=''
export COMPONENT=''

ALARM_TABLE=$(echo $ALARM_TABLE | tr " " "\n" | sort -k2 -n | head -$HEADCOUNT | sort -k8 -r)

for SAT_ID in $ALARM_PENDING_SAT_ID
do
	export each=''
	for each in $ALARM_TABLE
	do
		JSON_LINE_COUNTER=$(echo $each | grep "|$SAT_ID|")
		for each in $JSON_LINE_COUNTER
		do
			TOTAL_JSON_NODES=$((TOTAL_JSON_NODES+1))
		done
	done
done

for SAT_ID in $ALARM_PENDING_SAT_ID
do
alarm_output_start
	for ALARM_TABLE_CURRENT_LINE in $ALARM_TABLE
	do
	COMPONENT=$(echo $ALARM_TABLE_CURRENT_LINE | cut -f8 -d "|")
	JSON_FEEDER=$(echo $ALARM_TABLE_CURRENT_LINE | grep "|$SAT_ID|" | grep $COMPONENT)
	case "$COMPONENT" in
		BATT)
			SEVERITY="RED LOW"
		;;

		TSTAT)
			SEVERITY="RED HIGH"
		;;
	esac
	TIMESTAMP=$(echo $JSON_FEEDER | cut -f1 -d "|")
	if [[ $JSON_FEEDER != '' ]]
	then
		alarm_bracketing_open
		alarm_segment
		alarm_bracketing_close
	fi
	done
alarm_output_end
done
