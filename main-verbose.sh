#!/bin/bash

# The code here is a 'best-practice' that simply ensures garbage data isn't pulled
# into the script at runtime.  Variables that are defined here can be tweaked to
# change how the script acts at runtime.

############ DATA SANITIZERS ############
export SATELLITES=''
export NEWSAT=''
export SAT_ID=''
export SAT_INPUT=''
export SAT_COLLECTOR=''
export SAT_INPUT=''
export SAT_TABLE_CURRENT_LINE=''
export TIMESTAMP=''
export EPOCH=''
export NEXT_EPOCH=''
export CURRENT=''
export DATE=''
export DATE_TEST=''
export ALARM_SAT_TABLE=''
export HEADCOUNT='2'
export LENGTH='5min'
############ DATA SANITIZERS ############

# These are a safe, reliable way to display JSON.  As Bash, nor UNIX has a standard way
# to output JSON, I made my own based entirely on what I saw in the programming challenge.

############ JSON FUNCTIONS ############
alarm_output_start () {
echo "["
}

alarm_output_end () {
echo "]"
}

alarm_segment () {
echo '          satelliteId: '$SAT_ID','
echo '          severity: "'"$SEVERITY"'",'
echo '          component: "'"$COMPONENT"'",'
echo '          timestamp: "'"$JSON_YEAR-$JSON_MONTH-$JSON_DAY"T"$TIMESTAMP"Z""'"'
TOTAL_JSON_NODES=$((TOTAL_JSON_NODES-1))
}

alarm_bracketing_open () {
echo "     {"
}

alarm_bracketing_close () {
if [[ $TOTAL_JSON_NODES -gt 0 ]]
	then
		echo "     },"
	else
		echo "     }"
fi
}
############ JSON FUNCTIONS ############

# It is not clear from the input file if the date is in YYYYMMDD or YYYYDDMM format
# It might be a good change to the challenge itself if one of the fields was set to
# 30 so it'd be unmistakeable.
# For the purposes of this challenge, I will assume that the format is YYYYMMDD

############ DATE CODE ############
DATES=$(cat input.db | cut -f1 -d " ")
DATE_TEST=$(echo "$DATES" | sort | uniq -u)
echo $DATE_TEST
if [[ $DATE_TEST == '' ]]
then
	ALL_DATES_SAME='1'
else
	echo "GENERATING DATE DIRECTORIES FOR NEW DATES"
fi
for DAY in $DATES
do
        if [[ $DAYS != *"$DAY"* ]]
        then
        	DAYS="$DAY $DAYS"
        fi
done

for CURRENT_DAY in $DAYS
do
	JSON_YEAR=${CURRENT_DAY:0:4}
	JSON_MONTH=${CURRENT_DAY:4:2}
	JSON_DAY=${CURRENT_DAY:6:2}
done
############ DATE CODE ############

# In regards to logging and organizing data for alerts -
# I would always suggest generating a directory for each day,
# then populate file(s) within that directory.
# I'd header the file with the datestamp of its parent directory
# in case it ever got orphaned.
#
# Without getting too far in the weeds, something like this could be easily done to start off
# a 'filesystem' for logs/ingress but these will be comments out.  You could build the whole
# script within this loop and break out on each new day.
#
# for dir in $DAYS
#	do
#	mkdir $dir
#	done
#
# TIME MANAGEMENT - it should be noted that I elected to split up the table by Satellite ID
# first because I knew, regardless of anything else, it would be convenient to be able to
# "walk" the data for a given SAT_ID without stumbling over unwanted data. We know we have
# a date and a timestamp.  We know we have Satellite IDs by which our alert system will
# identify issues.  Looking at our data, it's easy for a person to tell that we are not
# going to need time accounting code or any way to manage the time related data.  All conditions
# given are within 5 minutes of the start time, and it's all on the same day.  But, in the
# real world, we absolutely would.  I've writen this whole thing without so much as touching
# the datestamp except when I use 'cut' to lob it off to prevent it from confusing other bash
# functions.  Full disclosure, I'm adding the date collector to this project last because even
# in the real world, I'd have run the script differently so that each day got its own log file,
# named by day upon execution.

############ ISOLATE SAT_ID ############
SAT_INPUT=$(cat input.db | cut -f2 -d "|")
for SAT_COLLECTOR in $SAT_INPUT
do
        if [[ $SATELLITES != *"$SAT_COLLECTOR"* ]]
        then
        	SATELLITES="$SAT_COLLECTOR $SATELLITES"
        fi
done
############ ISOLATE SAT_ID ############

# The above loops dynamically reveal the two SatIDs we're working with
# If there were an arbitrary number of more Satellites, this code would handle that
# A good "professional" data checking mechanism would be to ensure that the things going into
# SAT_INPUT were only four digits, and produce an error if it finds a letter, symbol, or too
# many digits.  The input data is beautiful in a way because the designer had the good sense
# to delimit most of the data with pipes.  Pipes are very grep-friendly.  This matters because
# what if, say, our timestamp contained "1000"?  If we just grepped the whole thing for "1000"
# we'd get lines we did not intend to get since we can search easily with pipes around the
# variable SAT_ID we can easily pick the right lines out of the input and analyze them.

############ MAIN LOOP ############
for SAT_ID in $SATELLITES
do

SAT_TABLE=$(cat input.db | grep "|$SAT_ID|" | cut -f2 -d " ")

	for SAT_TABLE_CURRENT_LINE in $SAT_TABLE
		do

# We have access to the current line of the sat table.
# With this, we can make comparisons and build rules
# using standard bash syntax.
# We want to look at a five minute interval so need to
# establish what "five minutes" means.  Then, using a few
# variables, we can test for a given condition.
# We know all of our dates are the same length so our
# timestamp is always going to be the first
# 12 characters of a given input line.
# This is one of the few times we can safely utilize
# parameter expansion, so I went for it.
		
############ TIME CODE ############
		TIMESTAMP=${SAT_TABLE_CURRENT_LINE:0:12}
		if [[ NEXT_EPOCH != '' ]]
		then
		EPOCH=$NEXT_EPOCH
		fi
		if [[ $EPOCH == '' ]]
		then
			EPOCH=$TIMESTAMP
			CURRENT=$TIMESTAMP
			WINDOW=$(date -d "$EPOCH $LENGTH" +"%H:%M:%S.%3N")
		else
			CURRENT=$TIMESTAMP
		fi
                CURRENT_UTIME=$(date -u -d "$CURRENT" +"%s.%3N")
                WINDOW_UTIME=$(date -u -d "$WINDOW" +"%s.%3N")

			if (( $(echo "$CURRENT_UTIME < $WINDOW_UTIME" | bc -l) ))
			then
############ TIME CODE ############

# To make actual DATESTAMPING work, we'd probably have to reparse the table to replace
# the first space with a "|" prior to the rule execution.  
#
# The reason this is an issue at all is because Bash naturally takes a space
# as a new-item.  The lines we have in the table, to bash, appear as a date,
# then everything else as the 'next' element.
# There's a tricky way that you could "step" over every other element to work
# around this, but I feel it would be unsafe in production, so I did it differently.

# Case-Statements are a great fit for looking at different components.
# In our situation, this is much easier on the eyes and far more
# maintainable than nested-if statements or the like.
# Here we could pull/push any field from any line of the table.
# So with just standard bash, we have a somewhat flexible, maybe even
# overkill solution than the robustness immediately required by
# this project, but it's a good framework to extend from.

############ ADDITIONAL RULE CODE ############
			COMPONENT=$(echo $SAT_TABLE_CURRENT_LINE | cut -f8 -d "|")

				case "$COMPONENT" in
						BATT)
							GET_RED_LOW=$(echo $SAT_TABLE_CURRENT_LINE | cut -f6 -d "|")
							GET_RAW=$(echo $SAT_TABLE_CURRENT_LINE | cut -f7 -d "|")
							if (( $(echo "$GET_RED_LOW > $GET_RAW" | bc -l) ))
								then
								BATT_ALARMDB="$SAT_TABLE_CURRENT_LINE $BATT_ALARMDB"
							fi
						;;
						TSTAT)
							GET_RED_HIGH=$(echo $SAT_TABLE_CURRENT_LINE | cut -f3 -d "|")
							GET_RAW=$(echo $SAT_TABLE_CURRENT_LINE | cut -f7 -d "|")
	        	            if (( $(echo "$GET_RED_HIGH < $GET_RAW" | bc -l) ))
                        		then
                               	TSTAT_ALARMDB="$SAT_TABLE_CURRENT_LINE $TSTAT_ALARMDB"
                            fi
						;;

							*) # This is where DEV OPS would add a new COMPONENT type
						echo "COMPONENT TYPE UNRECOGNIZED"
				esac
			else
				echo "THIS TIMESTAMP IS OUTSIDE THE ALLOTED TIME WINDOW - IT WILL BE USED FOR THE NEXT ITERATION OF RULE EVALUATIONS"
				NEXT_EPOCH=$CURRENT
			fi
	done
export EPOCH=''
done
############ ADDITIONAL RULE CODE ############
############ MAIN LOOP ############

############ ESTABLISHING TABLES ############
ALARM_TABLE=$TSTAT_ALARMDB" "$BATT_ALARMDB
############ ESTABLISHING TABLES ############

# Upon reflection, I'm glad I generate component tables seperately and then combine them
# because when someone adds a new component type, it'll be easy to add into the mix here.
# There may be a cleaner way to write this where we all use the same table, but that gets
# trickier on the back end, grepping things out all the time, which we already need to do.
# Digesting data so bash can work on it correctly is probably 80% of bash programming.

# The order has been changed to satisfy JSON output order preference
# Going forward, this actually makes it really easy to shift which 
# components get displayed in order.

############ RESANITIZING ############
export SAT_ID=''
############ RESANITIZING ############

############ COUNTING ALARMS ############
for SAT_ID in $SATELLITES
do
	for ALARM_TABLE_CURRENT_LINE in $ALARM_TABLE
	do
		ALARM_SAT_COUNTER=$(echo $ALARM_TABLE_CURRENT_LINE | grep "|$SAT_ID|" )
		if  [[ $ALARM_SAT_COUNTER != '' ]]
		then
			SAT_COUNT=$((SAT_COUNT+1))
		fi
	done
if (( "$SAT_COUNT" >= "3" ))
then
	ALARM_PENDING_SAT_ID=$SAT_ID" "$ALARM_PENDING_SAT_ID
fi
export SAT_COUNT=''
done
############ COUNTING ALARMS ############

# Here we're sifting through the boiled-down list of satellites that reported errors.
# The objective is to count how many blerbs we have from each satellite to see if we're
# meeting the "3 events on the same satellite" stipulation.
# 1001, clearly the superior satellite, should be dropped off by these loops
# because 1001 only reports one error.  At the end of these loops, we're left with
# a SAT_ID of the more panic-stricken satellite(s).

############ RESANITIZING ############
export SAT_ID=''
export ALARM_TABLE_CURRENT_LINE=''
export COMPONENT=''
############ RESANITIZING ############

############ JSON DISPLAY CODE ############

############ OUTPUT RESTRICTION ############
ALARM_TABLE=$(echo $ALARM_TABLE | tr " " "\n" | sort -k2 -n | head -$HEADCOUNT | sort -k8 -r)
############ OUTPUT RESTRICTION ############

############ JSON OBJECT COUNTER ############
for SAT_ID in $ALARM_PENDING_SAT_ID
do
	export each=''
	for each in $ALARM_TABLE
	do
		JSON_LINE_COUNTER=$(echo $each | grep "|$SAT_ID|")
		for each in $JSON_LINE_COUNTER
		do
			TOTAL_JSON_NODES=$((TOTAL_JSON_NODES+1))
		done
	done
done
############ JSON OBJECT COUNTER ############

for SAT_ID in $ALARM_PENDING_SAT_ID
do
alarm_output_start
	for ALARM_TABLE_CURRENT_LINE in $ALARM_TABLE
	do
	COMPONENT=$(echo $ALARM_TABLE_CURRENT_LINE | cut -f8 -d "|")
	JSON_FEEDER=$(echo $ALARM_TABLE_CURRENT_LINE | grep "|$SAT_ID|" | grep $COMPONENT)
	case "$COMPONENT" in
		BATT)
			SEVERITY="RED LOW"
		;;

		TSTAT)
			SEVERITY="RED HIGH"
		;;
	esac
	TIMESTAMP=$(echo $JSON_FEEDER | cut -f1 -d "|")
	if [[ $JSON_FEEDER != '' ]]
	then
		alarm_bracketing_open
		alarm_segment
		alarm_bracketing_close
	fi
	done
alarm_output_end
done
############ JSON DISPLAY CODE ############

# The Severity field is a strange one because it seems unnecessary to always have the
# same "severity" associated with the same component type.  
# While writing this, I realized using case statements where the rules are defined would
# make them extensible to support different ways of looking at/pulling severities, so I
# went back and removed the nested-if statements and replaced it with a much cleaner
# case statement.  As the scope of this project only explicitly shows BATTs being
# RED LOW and TSTATs being RED HIGHS, I'm writing it on that assumption.
# However I felt it important to note customer's needs change and it's important to build
# in ladders to get you out of holes later :)
